from app.api import api
from app import Database
import json
from flask import redirect, url_for, request


# Syed Review / Comments Code DATABASE PORTION

# @api.route('/get_comment', methods=["GET"])
def get_comment(movie_id):  # grabbing movie id
    # movie_id = request.values.get('movieID')
    myDB = Database.dbConnection()  # connects to database
    print(movie_id)
    sqlString = "Select userName, review from comments where movie_id = '" + movie_id + "'"
    result = Database.selectStatement(myDB, sqlString)
    cast_fetch = result.fetchall()  # pulls list of results rows
    completeInfo = {}
    count = 0
    if cast_fetch != 0:  # if not empty, continue
        for row in cast_fetch:  # for loop pulls values from results and puts them into new arr
            count += 1
            completeInfo[count] = {
                "name": row[0],
                "comment": row[1]
            }

        # }
    return completeInfo  # return the clean results of the arr


@api.route('/submit_comment', methods=["POST"])  # This entire sequence adds comment to database
def create_comment():  # Pulling data from post
    movie_id = request.values.get('movieID')  # assigns values to variables
    review = request.form.get('content', None)
    name = request.form.get('name', None)
    myDB = Database.dbConnection()  # connection to database

    sqlString = "Insert INTO comments (movie_id, review, userName) VALUES (%s,%s,%s)"
    values = (movie_id, review, name)
    result = Database.execStatement(myDB, sqlString, values)
    myDB.commit()
    res = {"res": "comment successful"}  # debugging code

    return redirect(url_for('index'))  # redirects back to home page